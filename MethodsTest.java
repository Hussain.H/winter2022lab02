public class MethodsTest{
	public static void main (String[]args){
		
		//Question 4
		
			int x =10;
			System.out.println(x);
			methodNoInputNoReturn();
			System.out.println(x);
			
		//Question 5
		
			methodOneInputNoReturn(10);
			methodOneInputNoReturn(x);
			methodOneInputNoReturn(x+50);
		
		//Question6
		
			methodTwoInputNoReturn(10,9.99);
		
		//Question 7
		
		int z = methodNoInputReturnInt();
		System.out.println("Inside the variable z");
		System.out.println(z);
		
		//Question 8
		
		double root = sumSquareRoot(6,3);
		System.out.println("Inside the variable root");
		System.out.println(root);
		
		//Question 10
		
		String s1="java";
		String s2="programming";
		System.out.println(s1.length());
		System.out.println(s1.length());
		
		//Question 11
		
		int m1 =SecondClass.addOne(50);
		System.out.println(m1);
		
		SecondClass sc = new SecondClass();
		int m2 = sc.addTwo(50);
		System.out.println(m2);
	}
	public static void methodNoInputNoReturn(){
		System.out.println("I’m in a method that takes no input and returns nothing");
		int x =50;
		System.out.println(x);
	}
	public static void methodOneInputNoReturn(int input1){
		System.out.println("Inside the method one input no return");
		System.out.println(input1);
		
	}
	public static void methodTwoInputNoReturn(int number1, double number2){
	System.out.println("Inside the method two no return");
	System.out.println("int: "+ number1+", double: "+ number2);
	}
	
	public static int methodNoInputReturnInt(){
	int x=6;
	return x;	
	}
	
	public static double sumSquareRoot(int number1, int number2){
		int result = number1+number2;
		double root = Math.sqrt(result);
		return root;
	}
}