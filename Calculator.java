public class Calculator{
	
	public static double add(double number1, double number2){
		double result = number1 + number2;
		return result;
	
	}
	
	public double subtract(double number1, double number2){
		double result  = number1 -number2;
		return result;
	}
	
	public static double multiply (double number1, double number2){
		double result = number1*number2;
		return result;
	}
	
	public double divide(double number1, double number2){
		double result = number1/number2;
		return result;
	}
	
}